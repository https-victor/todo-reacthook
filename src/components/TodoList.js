import React, { useContext } from 'react';
import TodoListItem from './TodoListItem';
import TodoContext from '../context/todo/todoContext';

const TodoList = () => {
  const todoContext = useContext(TodoContext);
  const { todos } = todoContext;
  return (
    <div>
      {todos.map((todo, idx) => (
        <TodoListItem key={todo.id} title={todo.title} id={todo.id} idx={idx} />
      ))}
    </div>
  );
};
export default TodoList;
