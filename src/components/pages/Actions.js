import React from 'react';
import AddTodoBtn from '../AddTodoBtn';
import RemoveAllBtn from '../RemoveAllBtn';

const Actions = () => {
  return (
    <div className='actions' style={{ display: 'flex' }}>
      <AddTodoBtn />
      <RemoveAllBtn />
    </div>
  );
};

export default Actions;
