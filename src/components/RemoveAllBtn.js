import React, { Fragment, useContext } from 'react';
import { Button } from 'antd';
import TodoContext from '../context/todo/todoContext';

const RemoveAllBtn = () => {
  const todoContext = useContext(TodoContext);

  const handleOnClick = () => {
    todoContext.delAll();
  };

  return (
    <Fragment>
      <Button
        type='danger'
        icon='delete'
        shape='circle'
        onClick={handleOnClick}
      />
    </Fragment>
  );
};

export default RemoveAllBtn;
