import React, { useState, useContext } from 'react';
import TodoContext from '../context/todo/todoContext';
import { Alert } from 'antd';

const AddTodoBtn = () => {
  const todoContext = useContext(TodoContext);
  const { addTodo, setWarning, alert } = todoContext;
  const [text, setText] = useState('');

  const handleOnClick = () => {
    text
      ? addTodo(text)
      : setWarning('Please, enter a title for the new todo', 'error');
  };
  const handleOnChange = e => {
    setText(e.target.value);
  };

  return (
    <div>
      {alert && <Alert message={alert.msg} type={alert.type} />}
      <input
        type='text'
        name='text'
        placeholder='Add Todo...'
        value={text}
        onChange={handleOnChange}
      />
      <button onClick={handleOnClick}>Add Todo</button>
    </div>
  );
};

export default AddTodoBtn;
