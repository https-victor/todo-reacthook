import React, { useContext, useState, useEffect } from 'react';
import { Button, Input } from 'antd';
import TodoContext from '../context/todo/todoContext';
// import useToggleMode from './useToggleMode';

const TodoListItem = ({ title, id, idx }) => {
  const todoContext = useContext(TodoContext);
  const { delTodo, editTodo } = todoContext;
  const [newTitle, setNewTitle] = useState(title);
  const [mode, setMode] = useState(false);

  useEffect(() => {
    editTodo(idx, newTitle);
    // eslint-disable-next-line
  }, [newTitle]);

  function toggleMode() {
    setMode(!mode);
  }
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {mode ? (
        <div>
          <Input
            onChange={e => {
              setNewTitle(e.target.value);
            }}
            value={newTitle}
          />
          <h1>Actual Title in State: {title}</h1>
        </div>
      ) : (
        <div style={{ display: 'flex' }}>
          <h1>{title}</h1>
        </div>
      )}

      <Button icon='delete' shape='circle' onClick={() => delTodo(id)} />
      <Button icon='edit' shape='circle' onClick={toggleMode} />

      {Date.now()}
    </div>
  );
};

export default TodoListItem;
