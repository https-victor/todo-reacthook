import React, { useContext } from 'react';
import { Alert } from 'antd';
import TodoContext from '../context/todo/todoContext';

const Warning = () => {
  const todoContext = useContext(TodoContext);
  const { alert } = todoContext;
  return alert !== null && <Alert message={alert.msg} type={alert.type} />;
};

export default Warning;
