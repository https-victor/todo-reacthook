import { useState, useEffect } from 'react';

const useToggleMode = (title, editTodo, idx) => {
  const [mode, setMode] = useState(false);
  const [newTitle, setNewTitle] = useState(title);

  useEffect(() => {
    !mode && editTodo(idx, newTitle);
  }, [mode]);
  function toggleMode() {
    setMode(!mode);
  }

  return [mode, toggleMode];
};
export default useToggleMode;
