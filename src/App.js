import React from 'react';
import './App.css';
import Actions from './components/pages/Actions';
import TodoList from './components/TodoList';
import TodoState from './context/todo/TodoState';

const App = () => {
  return (
    <TodoState>
      <Actions />
      <TodoList />
    </TodoState>
  );
};

export default App;
