export const ADD_TODO = 'addTodo';
export const DEL_TODO = 'delTodo';
export const DEL_ALL = 'delAll';
export const EDIT_TODO = 'editTodo';
export const WARNING = 'warning';
export const REMOVE_WARNING = 'removewarning';
