import React, { useReducer } from 'react';
import TodoReducer from './todoReducer';
import TodoContext from './todoContext';
import {
  ADD_TODO,
  DEL_TODO,
  DEL_ALL,
  EDIT_TODO,
  WARNING,
  REMOVE_WARNING
} from '../actionTypes';

const TodoState = props => {
  const initialState = {
    todos: [],
    alert: null
  };

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  const addTodo = title => {
    dispatch({ type: ADD_TODO, payload: title });
  };

  const setWarning = (msg, type) => {
    dispatch({ type: WARNING, payload: { msg, type } });
    setTimeout(() => {
      dispatch({ type: REMOVE_WARNING });
    }, 3000);
  };

  const delTodo = id => {
    dispatch({ type: DEL_TODO, payload: id });
  };

  const delAll = () => dispatch({ type: DEL_ALL });

  const editTodo = (idx, newTitle) => {
    dispatch({ type: EDIT_TODO, payload: { idx, newTitle } });
  };

  return (
    <TodoContext.Provider
      value={{
        todos: state.todos,
        alert: state.alert,
        addTodo,
        delTodo,
        delAll,
        editTodo,
        setWarning
      }}
    >
      {props.children}
    </TodoContext.Provider>
  );
};

export default TodoState;
