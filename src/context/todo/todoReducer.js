import {
  ADD_TODO,
  DEL_TODO,
  DEL_ALL,
  EDIT_TODO,
  WARNING,
  REMOVE_WARNING
} from '../actionTypes';

export default (state, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, { title: action.payload, id: Date.now() }]
      };
    case DEL_TODO:
      return {
        ...state,
        todos: state.todos.filter(todo => todo.id !== action.payload)
      };
    case DEL_ALL:
      return {
        ...state,
        todos: []
      };
    case EDIT_TODO:
      return {
        ...state,
        todos: [
          ...state.todos.slice(0, action.payload.idx),
          {
            ...state.todos[action.payload.idx],
            title: action.payload.newTitle
          },
          ...state.todos.slice(action.payload.idx + 1)
        ]
      };
    case WARNING:
      return {
        ...state,
        alert: { msg: action.payload.msg, type: action.payload.type }
      };
    case REMOVE_WARNING:
      return {
        ...state,
        alert: null
      };
    default:
      return state;
  }
};
